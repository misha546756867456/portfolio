import React, {useEffect, useRef} from 'react';
import '../App.css'

const TodoForm = ({onSubmit}) => {
	const [input, setInput] = React.useState('');

	const inputRef = useRef(null);

	useEffect(() => {
		inputRef.current.focus();
	});

	const handleChange = (e) => {
		setInput(e.target.value);
	};

	const handleSubmit = e => {
		e.preventDefault();

		onSubmit({
			id: Math.floor(Math.random() * 10000),
			text: input
		});

		setInput('');
	};

	return (
		<form className="todo-form"
		      onSubmit={handleSubmit}>
			<input type="text"
			       placeholder="Add a todo"
			       value={input}
			       name="text"
			       className='todo-input'
			       onChange={handleChange}
			       ref={inputRef}
			/>
			<button className="todo-button">Add todo</button>
		</form>
	);
};

export default TodoForm;