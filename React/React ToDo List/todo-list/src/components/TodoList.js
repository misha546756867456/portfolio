import React from 'react';
import TodoForm from "./TodoForm";
import Todo from "./Todo";

const TodoList = () => {
	const [todos, setTodos] = React.useState([]);

	const addTodo = todo => {
		if (!todo.text || /^\s*$/.test(todo.text)) {
			return;
		}
		const newTodos = [todo, ...todos];
		setTodos(newTodos);
	};

	const removeTodo = id => {
		console.log(id);
		console.log([...todos]);
		const removeArr = [...todos].filter(todo => todo.id !== id);

		setTodos(removeArr);
	};

	const updateTodo = (todoId, newValue) => {
		if (!newValue.text || /^\s*$/.test(newValue.text)) {
			return;
		}
		console.log(todoId, newValue.id);

		setTodos(prev => prev.map(item => (
			item.id === todoId ? newValue : item
		)))
	};

	const completeTodo = id => {
		let updatedTodos = todos.map(todo => {
			if (todo.id === id) {
				todo.isComplete = !todo.isComplete;
			}
			return todo;
		});
		setTodos(updatedTodos);
	};

	return (
		<div>
			<h1>What's the Plan for Today?</h1>
			<TodoForm onSubmit={addTodo}/>
			<Todo todos={todos} completeTodo={completeTodo} removeTodo={removeTodo} updateTodo={updateTodo}/>
		</div>
	);
};
export default TodoList;