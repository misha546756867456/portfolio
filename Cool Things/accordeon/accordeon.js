const headers = document.querySelectorAll('[data-name="accordeon-title"]');

headers.forEach((item) => {
    item.addEventListener('click', headerClick);
});

function headerClick() {
    this.nextElementSibling.classList.toggle("accordeon-hid");
}   